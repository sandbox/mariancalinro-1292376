This is an extension of the Boxes module ( http://drupal.org/project/boxes ) which has an image field and a link field.

You can create an image box which can be used as a commercial (i.e: banner image and link) , or static non linked images to be displayed on a sidebar. You can select the Image Style you want for the block.

The really useful stuff here is that you can edit the content inside the interface, on the block edit page, or inplace.