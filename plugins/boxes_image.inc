<?php

/**
 * Simple custom text box.
 */
class boxes_image extends boxes_box {
  /**
   * Implementation of boxes_box::options_defaults().
   */
  public function options_defaults() {
    return array(
      'box_id' => array(
        'value' => '',
      ),
      'image' => array(
        'fid' => '',
      ),
      'style' => array(
        'value' => '',
      ),
      'url' => array(
        'value' => '',
      ),
    );
  }

  /**
   * Implementation of boxes_box::options_form().
   */
  public function options_form(&$form_state) {
      $validators = array();
      $validators['file_validate_is_image'] = array();
      $validators['file_validate_extensions'] = array('jpg jpeg gif png');
      
      if (is_array($this->options['box_id'])){
        $box_id = $this->options['box_id']['value'] ? $this->options['box_id']['value'] : NULL;
      }
      else {
        $box_id = $this->options['box_id'] ? $this->options['box_id'] : NULL;
      }
      if (empty($box_id)){
        $box_id = variable_get("boxes_images_next_id", 1);
        variable_set("boxes_images_next_id", $box_id ++);
      }
      
      $form['box_id'] = array(
        '#type' => 'hidden',
        '#value' => $box_id,
      );
      
      $form['image'] = array(
        '#title' => t('Choose an image'),
        '#type' => 'managed_file',
        '#description' => t('Please add the image for this block.'),
        '#default_value' => $this->options['image'] ? $this->options['image'] : NULL,
        '#upload_location' => 'public://box_images/',
       	'#upload_validators' => $validators,
        '#required' => TRUE,
      );
      
      $styles = image_styles();
      $options = array();
      foreach ($styles as $key=>$style){
        $options[$key] = $style['name'];
      }
      $form['style'] = array(
        '#type' => 'select',
        '#title' => t('Choose Image Style'),
        '#default_value' => $this->options['style'] ? $this->options['style'] : NULL,
        '#options' => $options,
        '#description' => t('The image style that will be applyed to the uploaded image.'),
        '#required' => TRUE,
      );
      
      $form['url'] = array(
        '#type' => 'textfield',
        '#title' => t('Optional URL'),
        '#description' => t('If you input an URL here the image will be displayed as a link.'),
  		'#default_value' => $this->options['url'] ? $this->options['url'] : NULL,
        '#size' => 60,
        '#maxlength' => 128,
      );
      
      $form['#box_type'] = 'boxes_image';
      
      return $form;
  }


  /**
   * Implementation of boxes_box::render().
   */
  public function render() {
    $title = isset($this->title) ? check_plain($this->title) : NULL;
    
    if ($this->options['image']){
/*       An associative array containing:
 *   - style_name: The name of the style to be used to alter the original image.
 *   - path: The path of the image file relative to the Drupal files directory.
 *     This function does not work with images outside the files directory nor
 *     with remotely hosted images.
 *   - alt: The alternative text for text-based browsers.
 *   - title: The title text is displayed when the image is hovered in some
 *     popular browsers.
 *   - attributes: Associative array of attributes to be placed in the img tag.
 */
      $file = file_load($this->options['image']);
      $variables = array(
        'style_name' => $this->options['style'],
        'path'=> $file->uri,
        'alt' => $title,
        'title' => $title,
        'attributes' => array(),
      );
       $image = theme('image_style',$variables);
       $url = url($this->options['url']);
       $url_stripped = $url;
       $pos = strpos($url_stripped,'?');
       if ($pos !== FALSE){
         $url_stripped = substr($url_stripped, 0, $pos);
       }
       $pos = strpos($url_stripped,'%3F');
       if ($pos !== FALSE){
         $url_stripped = substr($url_stripped, 0, $pos);
       }
       if ($url_stripped != '' && $url_stripped != '/' && $url_stripped != '//'){
         $content = l($image,$url,array('html'=>true));
       }
       else {
         $content = $image;
       }
    }
    else {
      $content = '';
    } 

    return array(
      'delta' => $this->delta, // Crucial.
      'title' => '',//$title,
      'subject' => '',//$title,
      'content' => $content,
    );
  }
}

